/*
* Created by kukki, 05 Aug 2021.
* Extended L-System (https://en.wikipedia.org/wiki/L-system) with support of the second order rules.
* Can be used to imitate stochastic processes and fractal-like systems.
* You can check the interactive demo on https://www.shadertoy.com/view/7sdXRM.
*/

const float GRID_N = 400.;
const float PIXEL_SIZE = 1./ GRID_N;

const float rulesColWidth = 0.02; // col to display 02 rules

int startRule = 110; // initial o1 rule

#define ENABLE_O2 1
#define AUDIO_AS_A_RULE 0

// there are no chars, so o2 rules maps as follows: {0:' '}, {1:'<'}, {2:'>'}, {3:'+'}, {4:'-'}, {5:'|'}, {6:'!'}
const int o2N = 4;
const int o2Rules[o2N] = int[](4, 5, 3, 6); // o2 rules !! DOES NOT APPLIED WHEN SELECTED OPTION AUDIO_AS_A_RULE


// first row init types
#define AUDIO_AS_AN_INPUT 0
#define CENTER 1
#define ALTER 0


#define RYTHMIC_BAR 1

// how to init first line
float firstLine(vec2 coord)
{
#if AUDIO_AS_AN_INPUT

    // sample prev value if delayed
    const float samplingRate = 2.; 
    if(iFrame % int(GRID_N/samplingRate) > 0) // to have dalay by all lines for cycle use %
        return texture(iChannel1, coord).r;

    // an actual audio sample
    return float(texture(iChannel0, coord).r > 0.5 );

#elif CENTER
    return float(floor(coord.x * GRID_N) == floor(GRID_N/2.));
#elif ALTER
    return float(int(coord.x * GRID_N) & 1);
#endif
    return 0.;
}

// sample 3 cells in 'pixel' as binary 1 or 0
ivec3 sampleNeighboursBool(sampler2D sampler, vec2 coord)
{
   float th = 0.9;
   return ivec3(texture(sampler, coord - vec2(PIXEL_SIZE, 0)).r > th, 
                texture(sampler, coord).r > th,
                texture(sampler, coord + vec2(PIXEL_SIZE, 0)).r > th);
}

// o1 rule
int applyO1Rule(ivec3 neigh, int rule)
{
    int bitN = neigh.z | neigh.y << 1 | neigh.x << 2;
    return (rule >> bitN) & 1;
}

// -------- o2 rules ---------
const int ACTIVE_BITS = 8;
int circShift(int val, int shift)
{
    if(shift < 0)
        val = (val << (ACTIVE_BITS + shift)) | (val >> shift);
    else 
        val = (val >> (ACTIVE_BITS - shift)) | (val << shift);
    
    return val & ~(~0 << ACTIVE_BITS);
}
int circAdd(int val, int n)
{
   val += n;
   int limit = 256;
   if (val >= limit)
       val -= limit - 1;
   else if(val < 0)
       val = limit + val;
   return val;
}

int reverse(int val) {
   val =  (val & 0xF0) >> 4 | (val & 0x0F) << 4;
   val =  (val & 0xCC) >> 2 | (val & 0x33) << 2;
   return (val & 0xAA) >> 1 | (val & 0x55) << 1;
}

int bitNot(int val) {
    return ~val;
}
// ----------------------------------

// rule selection
void applyO2Rule(inout int o1Rule, int o2Rule)
{
    switch (o2Rule) {
       case 1: // '<'
           o1Rule = circShift(o1Rule,  1); break;
       case 2: // '>'
           o1Rule = circShift(o1Rule, -1); break;
       case 3: // '+'
           o1Rule = circAdd(o1Rule, 1);  break;
       case 4: // '-'
           o1Rule = circAdd(o1Rule, -1);  break;
       case 5: // '|'
           o1Rule = reverse(o1Rule);  break;
       case 6: // '!'
           o1Rule = bitNot(o1Rule);  break;
       }
}

int applyAllO2Rules(int o1Rule)
{
#if ! AUDIO_AS_A_RULE
   for(int i = 0; i < o2N; i++){
       applyO2Rule(o1Rule, o2Rules[i]);
   }
#endif
   return o1Rule;
}

// sample transformed o1 rule from texture
int getRule(vec2 coord, float line_h)
{
#if AUDIO_AS_A_RULE
    //sample prev value if delayed
    const float audioSamplingRate = floor(GRID_N/1.8);
    float intPart;
    if( modf(float(iFrame) / audioSamplingRate, intPart) > 1./audioSamplingRate)
        return int(texture(iChannel1, vec2(0., coord.y)).r * 255.);
        
    // rule sampling from audio
    return int(texture(iChannel0, vec2(0., coord.y)).r * 255.);
#endif
    // normalized float texture (unfortunately shadertoy doesn't have integers buff)
    return int(texture(iChannel1, vec2(0., coord.y + line_h)).r * 255.);
}

void mainImage( out vec4 fragColor, in vec2 fragCoord )
{
    float ratio = iResolution.x/iResolution.y;
    vec2 pixelsN = iResolution.xy /  vec2(GRID_N, GRID_N/ratio);

    // center coord (to avoid sampling errors on edges)
    fragCoord = floor(fragCoord/pixelsN) * pixelsN + pixelsN/2.;
    
    vec2 coord = fragCoord/iResolution.xy;
    
    // line height (ratio correction to match x size)
    float line_h = pixelsN.y/iResolution.y;
    
    int rule = startRule;
  
 #if ENABLE_O2
 
    // sample precalculated rule on prev line
    if(fragCoord.y < iResolution.y - pixelsN.y)
        rule = getRule(coord, line_h);
    
    // store o2 rule for the next step 
    if(coord.x < rulesColWidth){
        int nextRule = rule;
        if(fragCoord.y < iResolution.y - pixelsN.y)
            nextRule = applyAllO2Rules(nextRule);
        // store rule in r component and color code g and b for better color visualization
        fragColor = vec4( vec3(nextRule, nextRule>>4, nextRule&0xF)/vec3(255., 16., 16.), 1.);
        return;
    }
    
    // skip pixel if next to the rules column
    if(coord.x < rulesColWidth + PIXEL_SIZE){
        return;
    }
    
 #endif
    
    // init first line 
    if(fragCoord.y > iResolution.y - pixelsN.y)
    {
        float p = firstLine(coord);
        fragColor = vec4(p, p, p, 1.); // todo: rules for fancy colors
        return;
    }
    
    // cells for o1
    ivec3 col = sampleNeighboursBool(iChannel1, coord + vec2(0, pixelsN.y/iResolution.y));
    int p = applyO1Rule(col, rule);
    
    fragColor = vec4(vec3(p), 1.); // todo: rules for fancy colors
 
 #if RYTHMIC_BAR
     float samples = 40.;
     float ampl = texture(iChannel0, vec2(floor(coord.y * samples)/samples, 0.)).r;
     
     if(fragCoord.x/iResolution.x > 0.97)
         fragColor.rgb = mix(vec3(0, 0, 1), vec3(0,1,0), ampl);
 #endif
}
