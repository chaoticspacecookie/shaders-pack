/*
 * Created by kukki, 05 Dec 2022.
 */
 
#ifdef GL_ES
precision mediump float;
#endif
 
uniform float time;
uniform vec2 resolution;
 
vec2 po (vec2 v) {
	return vec2(length(v),atan(v.y,v.x));
}
vec2 ca (vec2 u) {
	return u.x*vec2(cos(u.y),sin(u.y));
}
float ln (vec2 p, vec2 a, vec2 b) { 
    float r = dot(p-a,b-a)/dot(b-a,b-a);
    r = clamp(r,0.,1.);
    p.x+=(0.7+0.5*sin(0.1*time))*0.2*smoothstep(1.,0.,abs(r*2.-1.))*sin(3.14159*(r-4.*time));
    return (1.+0.5*r)*length(p-a-(b-a)*r);
}
void mainImage( out vec4 Q, in vec2 U )
{   vec2 R = resolution.xy;
 	float r = 1e9;
 	U = 4.*(U-0.5*R)/R.y;
 	U.y += 1.5;
 	Q = vec4(0);
 	for (int i = 1; i < 20; i++) {
        U = ca(po(U)+0.3*(sin(2.*time)+0.5*sin(4.53*time)+0.1*cos(12.2*time))*vec2(0,1));
        r = min(r,ln(U,vec2(0),vec2(0,1.)));
        U.y-=1.;
        
        U.x=abs(U.x);
        U*=1.4+0.1*sin(time)+0.05*sin(0.2455*time)*(float(i));
        U = po(U);
        U.y += 1.+0.5*sin(0.553*time)*sin(sin(time)*float(i))+0.1*sin(7.*time)+0.05*sin(0.554*time);
        U = ca(U);
        
        
        Q+=sin(1.5*exp(-1e2*r*r)*1.4*vec4(1,-1.8,1.9,4)+time);
        
 		
 	}
 	Q/=18.;
}
 
void main(void)
{
    mainImage(gl_FragColor, gl_FragCoord.xy);
    gl_FragColor.a = 1.0;
}
